public class RaggedArray {
	
	public static void main(String[] args) {
      int[][] array = { 
                        {  1},
                        {  2,  3},
                        {  4,  5,  6},
                        {  7,  8,  9, 10},
                        { 11, 12, 13, 14, 15},
                        { 16, 17, 18, 19},
                        { 20, 21, 22},
                        { 23, 24},
                        { 25}
                      };
      
      System.out.println("Processing the array.");
      System.out.println("Total : " + getTotal(array));
      System.out.println("Average : " + getAverage(array));
      System.out.println("Total of row 0 : " + getRowTotal(array, 0));
      System.out.println("Total of row 2 : " + getRowTotal(array, 2));
      System.out.println("Total of col 0 : " + getColumnTotal(array, 0));
      System.out.println("Total of col 2 : " + getColumnTotal(array, 2));
      System.out.println("Highest in row 0 : " + getHighestInRow(array, 0));
      System.out.println("Highest in row 2 : " + getHighestInRow(array, 2));
      System.out.println("Lowest in row 0 : " + getLowestInRow(array, 0));
      System.out.println("Lowest in row 2 : " + getLowestInRow(array, 2));
      System.out.println("Highest in col 0 : " + getHighestInColumn(array, 0));
      System.out.println("Highest in col 2 : " + getHighestInColumn(array, 2));
      System.out.println("Lowest in col 0 : " + getLowestInColumn(array, 0));
      System.out.println("Lowest in col 2 : " + getLowestInColumn(array, 2));
      System.out.println("Elements in array : " + getElementCount(array));
	}
   
   public static int getTotal(int[][] array) {
      int total = 0;
      
      for (int row = 0; row < array.length; row++) {
         for (int col = 0; col < array[row].length; col++) {
            total = total + array[row][col];
         }   
      }
      
      return total;
   }

   public static double getAverage(int[][] array) {
	  double avg = 0;
	  avg = getTotal(array) / getElementCount(array);
      return avg;
   }

   public static int getRowTotal(int[][] array, int row) {
	  int total = 0;

	  for (int col = 0; col < array[row].length; col++) {
		  total = total + array[row][col];
	  }
	  
	  return total;
   }
   
   public static int getColumnTotal(int[][] array, int col) {
      int total = 0;
      
      for (int row = 0; row < array.length; row++) {
         if (array[row].length > col)
            total = total + array[row][col];
      }
      
      return total;
   }

  
   public static int getHighestInRow(int[][] array, int row) {
      int highest = array[row][0];
      
      for (int col = 0; col < array[row].length; col++) {
    	  if (array[row][col] > highest)
            highest = array[row][col];
      }
      
      return highest;
   }

   
   public static int getLowestInRow(int[][] array, int row) {
   		int lowest = array[row][0];

   		for (int col = 0; col < array[row].length; col++) {
        if (array[row][col] < lowest) {
           lowest = array[row][col];
        }
    }

    return lowest;
}

   
   public static int getHighestInColumn(int[][] array, int col) {
	      int highest = Integer.MIN_VALUE;
	   
	      for (int row = 0; row < array.length; row++)
	      {
	         if (col < array[row].length)
	            if(array[row][col] > highest)
	               highest = array[row][col];
	      }
	      
	      return highest;
   }

   public static int getLowestInColumn(int[][] array, int col) {
		      int lowest = Integer.MAX_VALUE;
		   
		      for (int row = 0; row < array.length; row++)
		      {
		         if (col < array[row].length)
		            if(array[row][col] < lowest)
		               lowest = array[row][col];
		      }
		      return lowest;
		   }
   
   public static int getElementCount(int[][] array) {
	   int count = 0;

	   for (int row = 0; row < array.length; row++) {
		   count = count + array[row].length;
	   }
	   
	   return count;
   }
   
}